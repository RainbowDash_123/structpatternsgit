// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StructPatternsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTPATTERNS_API AStructPatternsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
