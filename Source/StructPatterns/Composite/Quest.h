// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Task.h"
#include "Quest.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTPATTERNS_API UQuest : public UTask
{
	GENERATED_BODY()

public:
	//~~~Begin UTask interface
	virtual bool IsTaken() const override;
	virtual bool IsFinished() const override;
	virtual void Take() override;
	virtual void Finish() override;
	//~~~End UTask interface

	/** Add a task into chain of execution */
	void AddTask(UTask* Task);

private:
	/** All the dependent tasks which need to be executed for this quest to be finish */
	UPROPERTY()
		TArray<UTask*> Tasks;
};

inline bool UQuest::IsTaken() const
{
	//If any task is taken , the whole quest is
	for(auto Task : Tasks)
	{
		if(Task->IsTaken())
		{
			return true;
		}
	}

	return false;
}

inline bool UQuest::IsFinished() const
{
	//If we have no tasks, consider us done.
	bool bResult = true;
	
	//If all are done, we're done too.
	for(auto Task : Tasks)
	{
		bResult = Task->IsFinished();
	}

	return bResult;
}

inline void UQuest::Take()
{
	//Take the first non-taken
	for (auto Task : Tasks)
	{
		if(!Task->IsTaken())
		{
			return Task->Take();
		}
	}
}

inline void UQuest::Finish()
{
	/** Note: No super-class call on here, poor implementation may violate OCP and LSP */
	for(auto Task : Tasks)
	{
		/** Just finish it not to iterate through whole tree twice */
		Task->Finish();
	}
}

inline void UQuest::AddTask(UTask* Task)
{
	Tasks.Push(Task);
}


