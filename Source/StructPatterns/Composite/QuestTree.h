// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Quest.h"
#include "UObject/NoExportTypes.h"
#include "QuestTree.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTPATTERNS_API UQuestTree : public UObject
{
	GENERATED_BODY()

public:
	/**
	 * Accepts a quest scheme and invokes its parsing.
	 * @note The name hints, that this class is a "builder* pattern implementation
	 */
	void BuildTree(const FString& Scheme);

protected:
	/** in our case just spawn quest. */
	UQuest* ParseQuest();

	/** In our case just soawn task */
	UTask* ParseTask();

	/** Parses given quest scheme and creates a tree also setting QuestRoot */
	void ParseScheme(const FString& Scheme);

private:
	/** Quest tree root. */
	UPROPERTY()
		TArray<UQuest*> QuestRoot;
};

inline UQuest* UQuestTree::ParseQuest()
{
	return NewObject<UQuest>(this);
}

inline UTask* UQuestTree::ParseTask()
{
	return NewObject<UTask>(this);
}

inline void UQuestTree::BuildTree(const FString& Scheme)
{
	ParseScheme(Scheme);
}

inline void UQuestTree::ParseScheme(const FString& Scheme)
{
	TArray<UQuest*> Quests;
	FString Storage;

	//Straightforward symbol parsing
	for(auto Symbol : Scheme)
	{
		if(Symbol == '}')
		{
			Quests.Pop();
		}
		else if(Symbol == ':')
		{
			auto Quest = ParseQuest();
			if(Quests.Num()<=0)
			{
				QuestRoot = Quests;
			}
			else
			{
				Quests.Top()->AddTask(Quest);
			}
			Quests.Push(Quest);
		}
		else
		{
			Storage += Symbol;
			if(Storage.Contains("Task", ESearchCase::IgnoreCase))
			{
				auto Task = ParseTask();
				Quests.Top()->AddTask(Task);
				Storage = "";
			}
		}
	}
}



