// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Task.generated.h"

/**
 * Same simple task.
 */
UCLASS()
class STRUCTPATTERNS_API UTask : public UObject
{
	GENERATED_BODY()

public:
	/** @return bIsTaken */
	FORCEINLINE virtual bool IsTaken() const { return bIsTaken; }

	/** @return bIsFinished */
	FORCEINLINE virtual bool IsFinished() const { return bIsFinished; }

	/** Start executing this task. */
	virtual void Take();

	/** Finish this task executing */
	virtual void Finish();

private:
	/** True if task is in progress. */
	bool bIsTaken;

	/** True if task is finished */
	bool bIsFinished;
};

inline void UTask::Take()
{
	bIsTaken = true;
}

inline void UTask::Finish()
{
	bIsFinished = true;
}

